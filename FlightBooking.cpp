

#include "FlightBooking.h"

FlightBooking::FlightBooking(long id,double price,
                            string fromDate,string toDate, 
                            long tripID,long customerID,string customerName,
                            string fromDest,string toDest,string airline,string w_oder_a)

:Booking (id,price,fromDate,toDate,tripID,customerID,customerName)//call BookingIG constructor for id
{
    //cout<<w_oder_a<<endl;
    this->fromDest=fromDest;
    this->toDest=toDest;
    this->airline=airline;
    //this->window=w_oder_a;
    this->type='F';

//    if(w_oder_a=="W"){
//        this->window= "Fenster";
//    }
//    else{this->window= "Gang";}
    char seat=w_oder_a.at(0);
    switch (seat){
    case 'W':
    {
        this->window= "Fenster";
        break;
    }
    case 'A':
    {
        this->window= "Gang";
        break;
    }
    default:
    {
        window="default";
    }
}
}
vector<string> FlightBooking::getDetails(){
    vector<string> details;
    details.push_back(fromDest);
    details.push_back(toDest);
    details.push_back(airline);
    details.push_back(window);
    return details;
}
FlightBooking::~FlightBooking() {
}
string FlightBooking::showDetails(){
    if(window=="Fenster"){
        return "Fenster";
    }
    return "Gang";
}
void FlightBooking::print(){
    cout<<"FlightBooking "<<id<<endl;
}
