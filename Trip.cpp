

#include "Trip.h"

Trip::Trip(long tripID,long customerID) {
    this->id=tripID;
    this->customerID=customerID;
}

void Trip::addBooking(Booking * theBooking){
    relatedBookings.push_back(theBooking);
}
long Trip::getID(){
    return this->id;
}
long Trip::getCustomerID(){
    return this->customerID;
}
void Trip::printBookings(){
    cout<<"Trip with id "<<id<<" has "<<relatedBookings.size()<<" bookings"<<endl<<flush;
}

Trip::~Trip() {
}

