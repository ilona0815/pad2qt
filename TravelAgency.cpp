
#include "TravelAgency.h"
#include "Booking.h"
TravelAgency::TravelAgency() {
}

TravelAgency::~TravelAgency() {
}

vector<string> TravelAgency::getAllBookingStrings(){  // not necessary since no list widget??
    vector<string> allBigStrings;
    for(Booking* b:allBookings){
        allBigStrings.push_back(b->getBigString());
    }
    return allBigStrings;
}

vector<Booking*> TravelAgency::getAllBookings(){
    return this->allBookings;
}

int TravelAgency::createBooking(
        char type, double price, string fromDate, string toDate, long tripID, vector<string> details)
{
    if (findTrip(tripID) == NULL){return -1;} //if there's no such trip

    long correctID = Booking::getCurrentID();//takes id from static
    if(details.size()==5) {
        correctID=stoi(details.at(4));} //takes id from file, which is at the end of details

    //customer:
    int customerID=findTrip(tripID)->getCustomerID();
    string customerName = findCustomer(customerID)->getName();

    string one=details.at(0);
    string two=details.at(1);
    string three=details.at(2);
    string four="dummy string four";

    string test=details.at(3);
    if (test!="empty string"&&!isdigit(test.at(0))) { four=test;}

    switch (type) {
    case 'R':{
        CarReservation* cr = new CarReservation(
                    correctID, price, fromDate, toDate, tripID, customerID, customerName, one,two,three,four);
        findTrip(tripID)->addBooking(cr); //Booking added to the trip
        allBookings.push_back(cr);
        BookingsList.push_back(cr);
        break;
    }
    case 'H':{
        HotelBooking* hb = new HotelBooking(
                    correctID, price, fromDate, toDate, tripID, customerID, customerName, one, two,three);
        findTrip(tripID)->addBooking(hb); //Booking added to the trip
        allBookings.push_back(hb);
        BookingsList.push_back(hb);
        break;
    }
    case 'F':{
        FlightBooking* fb = new FlightBooking(
                    correctID, price, fromDate, toDate, tripID, customerID, customerName,one,two,three,four);
        findTrip(tripID)->addBooking(fb); //Booking added to the trip
        allBookings.push_back(fb);
        BookingsList.push_back(fb);
        break;
    }
    }
    return correctID;
}

Booking* TravelAgency::findBooking(long id) {
    for (Booking * bp : allBookings) {
        if (bp->getID() == id) {

            return bp;
        }
    }
    return NULL;
}

Trip* TravelAgency::findTrip(long id) {
    for (Trip* tp : allTrips) {
        if (tp->getID() == id) {
            return tp;
        }
    }
    return NULL;
}

Customer* TravelAgency::findCustomer(long id) {
    for (Customer* cp : allCustomers) {
        if (cp->getID() == id) {
            return cp;
        }
    }
    return NULL;
}

string TravelAgency::readfile() {
    string output;

    if(file_loaded){
        output = "File already loaded!";
        return output;

    }else{
        QString qFile = QFileDialog::getOpenFileName();
        string fileName =qFile.toStdString();

        ifstream myInputFile(fileName);
        //  ifstream myInputFile("./bookings_praktikum4_test.txt");
        string line;

        double priceAccumulator = 0;
        long carCounter=0;
        long hotelCounter=0;
        long flightCounter=0;
        long bookingCounter=0;

        while (getline(myInputFile, line)) {//get every line of the file
            bookingCounter++;
            istringstream streamOfLine(line);
            string part; //for the parts of the line
            vector<string> parsedBooking;
            char type='N';
            while (getline(streamOfLine, part, '|')) {//fill vector of strings
                try{
                    if(part==""||part=="\r"||part=="\n"||part=="\r\n"){throw 1;}
                    parsedBooking.push_back(part);
                    if(parsedBooking.size()==1) {
                        type = parsedBooking.at(0).at(0);
                    }
                }
                catch(int x){
                    cerr<< "err "<<x<<endl;
                    string reading_failed="Missing parameter number ";
                    reading_failed += to_string(parsedBooking.size()+1);
                    reading_failed += " in line number ";
                    reading_failed += to_string(bookingCounter);

                    bool needToEmpty=true;
                    while (needToEmpty){
                        if (allBookings.size()==0){needToEmpty=false;break;}else{
                            allBookings.pop_back();}
                    }
                    return reading_failed;
                }
            }

            //try catch with exceptions:
            try{
                //...
                int size = parsedBooking.size();
                if (type=='H'&& size<11){
                    throw std::runtime_error("HotelBooking is missing a parameter");
                }
                if (size<12 && type!='H'){
                    throw std::runtime_error("Non-HotelBooking is missing a parameter");
                }
            }
            catch(std::exception& e){
                cerr<< e.what() <<endl;
                string reading_failed="Missing parameter number ";
                reading_failed += to_string(parsedBooking.size()+1);
                reading_failed += " in line number ";
                reading_failed += to_string(bookingCounter);
                bool needToEmpty=true;
                while (needToEmpty){
                    allBookings.pop_back();
                    if (allBookings.empty()){needToEmpty=false;}
                }
                return reading_failed;
            }
            //general parameters for every booking:
            long id;
            double price;
            string fromDate;
            string toDate;
            long tripID;
            long customerID;
            string customerName;

            id=stoi(parsedBooking.at(2 - 1));
            price = stod(parsedBooking.at(3 - 1));
            fromDate = parsedBooking.at(4 - 1);
            toDate = parsedBooking.at(5 - 1);
            tripID = stoi(parsedBooking.at(6 - 1));
            customerID = stoi(parsedBooking.at(7 - 1));
            customerName = parsedBooking.at(8 - 1);
            priceAccumulator += price;

            //customers and trips creation:
            if (findTrip(tripID) == NULL)
            {
                Trip* tp = new Trip(tripID, customerID);
                allTrips.push_back(tp);
            }
            if (findCustomer(customerID) == NULL)
            {
                Customer* cp = new Customer(customerID, customerName);
                allCustomers.push_back(cp);
            }
            findCustomer(customerID)->addTrip(findTrip(tripID)); //assign trip to customer

            vector<string>details;
            details.push_back(parsedBooking.at(9 - 1));//From Dest - PickUp LOcation - Hotel
            details.push_back(parsedBooking.at(10 - 1));//To Dest  - Return Location - Town
            details.push_back(parsedBooking.at(11 - 1));//Airline  -   company       - smoke

            details.push_back( parsedBooking.size()>=12 ? parsedBooking.at(12 - 1) : "empty string") ;  //Window     -  insurance     - previous
            details.push_back( parsedBooking.size()>=13 ? parsedBooking.at(13 - 1) : "empty string") ;  //previous   -   previous     - previous
            details.push_back( parsedBooking.size()==14 ? parsedBooking.at(14 - 1) : "empty string") ;  //previous   -   previous     - nothing

            int emptycounter=0;
            for (auto i:details){
                if(i=="empty string"){
                    //cout<<"empty string found"<<endl;
                    emptycounter++;
                }
            }
            if(emptycounter>3){
                 cout<<"too empty for a hotel..."<<endl;
            }
            if(emptycounter>2&&type!='H'){
               cout<<"too empty..."<<endl;
            }


            details.push_back(to_string(id));
            createBooking(type, price, fromDate, toDate, tripID, details);

            //TODO: replace switch with counters inside class(es), beware that main needs to use them later
            switch (type) {
            case 'R':{
                carCounter++;
                break;}
            case 'H':{
                hotelCounter++;
                break;}
            case 'F':{
                flightCounter++;
                break;}
            default:{
                cerr << "unknown error while counting types";}
            }
        }

        output="Total value: "+to_string(priceAccumulator)+" EUR. "+"\n"
                +"Total number of all Customers: "+to_string(allCustomers.size())+
                +"\n""Total number of all Bookings: "+to_string(allBookings.size())+
                +"\n" "Total number of all Trips: "+to_string(allTrips.size());

        // findCustomer(1)->printTrips();
        //   findTrip(17)->printBookings();
        this->file_loaded = true;
        return output;
    }
}
