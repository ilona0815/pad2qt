#include "test.h"
#include "TravelAgency.h"

test::test(QObject *parent) :QObject(parent)
{

}
void test::rentalcars(){
    TravelAgency t;
    t.readfile();
    int amountofavisrentals= 0;
    vector<Booking*> b =  t.getAllBookings();
    for (Booking* booking: b){

        if(booking->getType() == 'R' && booking->getDetails().at(2) == "Avis")
            amountofavisrentals ++;
    }
    QCOMPARE(amountofavisrentals,5);
}
void test::flightbookings(){
    TravelAgency t;
    t.readfile();
    int flightswithunitedairlines= 0;
    vector<Booking*> b =  t.getAllBookings();
    for (Booking* booking: b){

        if(booking->getType() == 'F' && booking->getDetails().at(2) == "United Airlines")
            flightswithunitedairlines ++;
    }
    QCOMPARE(flightswithunitedairlines,3);
}
void test::bookings(){
    TravelAgency t;
    t.readfile();
    int bookingwithvalueover1000= 0;
    vector<Booking*> b =  t.getAllBookings();
    for (Booking* booking: b){

        if(booking->getPrice() >= 1000)
            bookingwithvalueover1000 ++;
    }
    QCOMPARE(bookingwithvalueover1000,31);

}
