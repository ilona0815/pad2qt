
#ifndef CUSTOMER_H
#define CUSTOMER_H
#include "general.h"
#include "Trip.h"
class Customer {
public:
    Customer(long,string);
    void addTrip(Trip* theTrip);
    long getID();
    string getName();
    void printTrips();
    virtual ~Customer();
private:
    vector<Trip*> myTrips;
    long id;
    string name;
};

#endif /* CUSTOMER_H */

