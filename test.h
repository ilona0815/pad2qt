#ifndef TEST_H
#define TEST_H
#include <QtTest/QTest>

class test: public QObject
{
    Q_OBJECT

private slots:
    void rentalcars();
    void flightbookings();
    void bookings();

public:
    explicit test(QObject *parent=0);
};

#endif // TEST_H
