

#include "CarReservation.h"

CarReservation::CarReservation(long id, double price,
                                string fromDate, string toDate,
                                long tripID, long customerID, string customerName,
                                string pickupLocation, string returnLocation, string company, string insurance)


:Booking (id,price,fromDate,toDate,tripID,customerID,customerName)//call BookingIG constructor for id
{
    //do the rest of constructor for car reservation
    this->pickupLocation=pickupLocation;
    this->returnLocation=returnLocation;
    this->company=company;
    this->insurance=insurance;
    this->type='R';
}

vector<string> CarReservation::getDetails(){
    vector<string> details;
    details.push_back(pickupLocation);
    details.push_back(returnLocation);
    details.push_back(company);
    details.push_back(insurance);
    return details;
}

string CarReservation::showDetails(){
    return insurance;
}

CarReservation::~CarReservation() {
}

void CarReservation::print(){
    cout<<"CarReservation "<<id<<endl;
}
