/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionRead_File;
    QAction *actionShow_bookings;
    QAction *actionClose_programm;
    QAction *actionTest1;
    QWidget *centralWidget;
    QTableWidget *tableWidget;
    QGroupBox *groupBox;
    QCalendarWidget *calendarWidget;
    QCalendarWidget *calendarWidget_2;
    QLabel *label_buchungsnummer;
    QTextBrowser *textBrowser_buchungsnummer;
    QLabel *label_preis;
    QLabel *label_reisenummer;
    QLabel *label_kunde;
    QTextBrowser *textBrowser_preis;
    QTextBrowser *textBrowser_reisenummer;
    QTextBrowser *textBrowser_kunde;
    QStackedWidget *stackedWidget;
    QWidget *page_3;
    QGroupBox *groupBox_details1;
    QTextBrowser *textBrowser_versicherung;
    QTextBrowser *textBrowser_abholort;
    QTextBrowser *textBrowser_ruckgabeort;
    QTextBrowser *textBrowser_firma;
    QLabel *label_firma;
    QLabel *label_abholort;
    QLabel *label_ruckgabeort;
    QLabel *label_versicherung;
    QWidget *page_4;
    QGroupBox *groupBox_details3;
    QLabel *label_kunde_2;
    QTextBrowser *textBrowser_sitzplatz;
    QTextBrowser *textBrowser_ziel;
    QTextBrowser *textBrowser_airline;
    QTextBrowser *textBrowser_abflug;
    QLabel *label_abflug;
    QLabel *label_sitzplatz;
    QLabel *label_airline;
    QLabel *label_ziel;
    QWidget *page_5;
    QGroupBox *groupBox_details2;
    QLabel *label_hotel;
    QLabel *label_stadt;
    QLabel *label_raucher;
    QTextBrowser *textBrowser_hotel;
    QTextBrowser *textBrowser_stadt;
    QMenuBar *menuBar;
    QMenu *menuReiseb_ro;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1103, 592);
        actionRead_File = new QAction(MainWindow);
        actionRead_File->setObjectName(QStringLiteral("actionRead_File"));
        actionShow_bookings = new QAction(MainWindow);
        actionShow_bookings->setObjectName(QStringLiteral("actionShow_bookings"));
        actionClose_programm = new QAction(MainWindow);
        actionClose_programm->setObjectName(QStringLiteral("actionClose_programm"));
        actionTest1 = new QAction(MainWindow);
        actionTest1->setObjectName(QStringLiteral("actionTest1"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tableWidget = new QTableWidget(centralWidget);
        if (tableWidget->columnCount() < 3)
            tableWidget->setColumnCount(3);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(10, 10, 351, 491));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(370, 10, 721, 491));
        calendarWidget = new QCalendarWidget(groupBox);
        calendarWidget->setObjectName(QStringLiteral("calendarWidget"));
        calendarWidget->setGeometry(QRect(420, 230, 304, 195));
        calendarWidget_2 = new QCalendarWidget(groupBox);
        calendarWidget_2->setObjectName(QStringLiteral("calendarWidget_2"));
        calendarWidget_2->setGeometry(QRect(420, 30, 304, 195));
        label_buchungsnummer = new QLabel(groupBox);
        label_buchungsnummer->setObjectName(QStringLiteral("label_buchungsnummer"));
        label_buchungsnummer->setGeometry(QRect(10, 30, 121, 31));
        textBrowser_buchungsnummer = new QTextBrowser(groupBox);
        textBrowser_buchungsnummer->setObjectName(QStringLiteral("textBrowser_buchungsnummer"));
        textBrowser_buchungsnummer->setGeometry(QRect(130, 30, 131, 31));
        label_preis = new QLabel(groupBox);
        label_preis->setObjectName(QStringLiteral("label_preis"));
        label_preis->setGeometry(QRect(10, 70, 41, 20));
        label_reisenummer = new QLabel(groupBox);
        label_reisenummer->setObjectName(QStringLiteral("label_reisenummer"));
        label_reisenummer->setGeometry(QRect(10, 100, 101, 20));
        label_kunde = new QLabel(groupBox);
        label_kunde->setObjectName(QStringLiteral("label_kunde"));
        label_kunde->setGeometry(QRect(10, 130, 62, 20));
        textBrowser_preis = new QTextBrowser(groupBox);
        textBrowser_preis->setObjectName(QStringLiteral("textBrowser_preis"));
        textBrowser_preis->setGeometry(QRect(70, 60, 191, 31));
        textBrowser_reisenummer = new QTextBrowser(groupBox);
        textBrowser_reisenummer->setObjectName(QStringLiteral("textBrowser_reisenummer"));
        textBrowser_reisenummer->setGeometry(QRect(130, 90, 131, 31));
        textBrowser_kunde = new QTextBrowser(groupBox);
        textBrowser_kunde->setObjectName(QStringLiteral("textBrowser_kunde"));
        textBrowser_kunde->setGeometry(QRect(70, 120, 191, 31));
        stackedWidget = new QStackedWidget(groupBox);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setGeometry(QRect(10, 160, 381, 311));
        page_3 = new QWidget();
        page_3->setObjectName(QStringLiteral("page_3"));
        groupBox_details1 = new QGroupBox(page_3);
        groupBox_details1->setObjectName(QStringLiteral("groupBox_details1"));
        groupBox_details1->setGeometry(QRect(0, 0, 321, 291));
        groupBox_details1->setFlat(false);
        textBrowser_versicherung = new QTextBrowser(groupBox_details1);
        textBrowser_versicherung->setObjectName(QStringLiteral("textBrowser_versicherung"));
        textBrowser_versicherung->setGeometry(QRect(120, 180, 181, 61));
        textBrowser_abholort = new QTextBrowser(groupBox_details1);
        textBrowser_abholort->setObjectName(QStringLiteral("textBrowser_abholort"));
        textBrowser_abholort->setGeometry(QRect(120, 60, 181, 61));
        textBrowser_ruckgabeort = new QTextBrowser(groupBox_details1);
        textBrowser_ruckgabeort->setObjectName(QStringLiteral("textBrowser_ruckgabeort"));
        textBrowser_ruckgabeort->setGeometry(QRect(120, 120, 181, 61));
        textBrowser_firma = new QTextBrowser(groupBox_details1);
        textBrowser_firma->setObjectName(QStringLiteral("textBrowser_firma"));
        textBrowser_firma->setGeometry(QRect(120, 30, 181, 31));
        label_firma = new QLabel(groupBox_details1);
        label_firma->setObjectName(QStringLiteral("label_firma"));
        label_firma->setGeometry(QRect(10, 40, 62, 20));
        label_abholort = new QLabel(groupBox_details1);
        label_abholort->setObjectName(QStringLiteral("label_abholort"));
        label_abholort->setGeometry(QRect(10, 80, 62, 20));
        label_ruckgabeort = new QLabel(groupBox_details1);
        label_ruckgabeort->setObjectName(QStringLiteral("label_ruckgabeort"));
        label_ruckgabeort->setGeometry(QRect(10, 140, 81, 20));
        label_versicherung = new QLabel(groupBox_details1);
        label_versicherung->setObjectName(QStringLiteral("label_versicherung"));
        label_versicherung->setGeometry(QRect(10, 200, 91, 20));
        stackedWidget->addWidget(page_3);
        page_4 = new QWidget();
        page_4->setObjectName(QStringLiteral("page_4"));
        groupBox_details3 = new QGroupBox(page_4);
        groupBox_details3->setObjectName(QStringLiteral("groupBox_details3"));
        groupBox_details3->setGeometry(QRect(0, 0, 321, 281));
        label_kunde_2 = new QLabel(groupBox_details3);
        label_kunde_2->setObjectName(QStringLiteral("label_kunde_2"));
        label_kunde_2->setGeometry(QRect(0, 130, 62, 20));
        textBrowser_sitzplatz = new QTextBrowser(groupBox_details3);
        textBrowser_sitzplatz->setObjectName(QStringLiteral("textBrowser_sitzplatz"));
        textBrowser_sitzplatz->setGeometry(QRect(120, 240, 181, 31));
        textBrowser_ziel = new QTextBrowser(groupBox_details3);
        textBrowser_ziel->setObjectName(QStringLiteral("textBrowser_ziel"));
        textBrowser_ziel->setGeometry(QRect(120, 100, 181, 61));
        textBrowser_airline = new QTextBrowser(groupBox_details3);
        textBrowser_airline->setObjectName(QStringLiteral("textBrowser_airline"));
        textBrowser_airline->setGeometry(QRect(120, 170, 181, 61));
        textBrowser_abflug = new QTextBrowser(groupBox_details3);
        textBrowser_abflug->setObjectName(QStringLiteral("textBrowser_abflug"));
        textBrowser_abflug->setGeometry(QRect(120, 30, 181, 61));
        label_abflug = new QLabel(groupBox_details3);
        label_abflug->setObjectName(QStringLiteral("label_abflug"));
        label_abflug->setGeometry(QRect(10, 40, 62, 20));
        label_sitzplatz = new QLabel(groupBox_details3);
        label_sitzplatz->setObjectName(QStringLiteral("label_sitzplatz"));
        label_sitzplatz->setGeometry(QRect(10, 250, 62, 20));
        label_airline = new QLabel(groupBox_details3);
        label_airline->setObjectName(QStringLiteral("label_airline"));
        label_airline->setGeometry(QRect(10, 180, 62, 20));
        label_ziel = new QLabel(groupBox_details3);
        label_ziel->setObjectName(QStringLiteral("label_ziel"));
        label_ziel->setGeometry(QRect(10, 110, 62, 20));
        stackedWidget->addWidget(page_4);
        page_5 = new QWidget();
        page_5->setObjectName(QStringLiteral("page_5"));
        groupBox_details2 = new QGroupBox(page_5);
        groupBox_details2->setObjectName(QStringLiteral("groupBox_details2"));
        groupBox_details2->setGeometry(QRect(0, 0, 321, 221));
        label_hotel = new QLabel(groupBox_details2);
        label_hotel->setObjectName(QStringLiteral("label_hotel"));
        label_hotel->setGeometry(QRect(10, 40, 41, 20));
        label_stadt = new QLabel(groupBox_details2);
        label_stadt->setObjectName(QStringLiteral("label_stadt"));
        label_stadt->setGeometry(QRect(10, 110, 41, 20));
        label_raucher = new QLabel(groupBox_details2);
        label_raucher->setObjectName(QStringLiteral("label_raucher"));
        label_raucher->setGeometry(QRect(50, 180, 121, 20));
        textBrowser_hotel = new QTextBrowser(groupBox_details2);
        textBrowser_hotel->setObjectName(QStringLiteral("textBrowser_hotel"));
        textBrowser_hotel->setGeometry(QRect(50, 30, 191, 61));
        textBrowser_stadt = new QTextBrowser(groupBox_details2);
        textBrowser_stadt->setObjectName(QStringLiteral("textBrowser_stadt"));
        textBrowser_stadt->setGeometry(QRect(50, 100, 191, 61));
        stackedWidget->addWidget(page_5);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1103, 25));
        menuReiseb_ro = new QMenu(menuBar);
        menuReiseb_ro->setObjectName(QStringLiteral("menuReiseb_ro"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuReiseb_ro->menuAction());
        menuReiseb_ro->addAction(actionRead_File);
        menuReiseb_ro->addAction(actionShow_bookings);
        menuReiseb_ro->addAction(actionClose_programm);
        menuReiseb_ro->addAction(actionTest1);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionRead_File->setText(QApplication::translate("MainWindow", "Read File", Q_NULLPTR));
        actionShow_bookings->setText(QApplication::translate("MainWindow", "Show bookings", Q_NULLPTR));
        actionClose_programm->setText(QApplication::translate("MainWindow", "Close programm", Q_NULLPTR));
        actionTest1->setText(QApplication::translate("MainWindow", "Test1", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "id", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "price", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "client", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindow", "Buchungsdetails", Q_NULLPTR));
        label_buchungsnummer->setText(QApplication::translate("MainWindow", "Buchungsnummer", Q_NULLPTR));
        label_preis->setText(QApplication::translate("MainWindow", "Preis", Q_NULLPTR));
        label_reisenummer->setText(QApplication::translate("MainWindow", "Reisenummer", Q_NULLPTR));
        label_kunde->setText(QApplication::translate("MainWindow", "Kunde", Q_NULLPTR));
        groupBox_details1->setTitle(QApplication::translate("MainWindow", "Rentalcar Details", Q_NULLPTR));
        label_firma->setText(QApplication::translate("MainWindow", "Firma", Q_NULLPTR));
        label_abholort->setText(QApplication::translate("MainWindow", "Abholort", Q_NULLPTR));
        label_ruckgabeort->setText(QApplication::translate("MainWindow", "Ruckgabeort", Q_NULLPTR));
        label_versicherung->setText(QApplication::translate("MainWindow", "Versicherung", Q_NULLPTR));
        groupBox_details3->setTitle(QApplication::translate("MainWindow", "Flight Deatils", Q_NULLPTR));
        label_kunde_2->setText(QString());
        label_abflug->setText(QApplication::translate("MainWindow", "Abflug", Q_NULLPTR));
        label_sitzplatz->setText(QApplication::translate("MainWindow", "Sitzplatz", Q_NULLPTR));
        label_airline->setText(QApplication::translate("MainWindow", "Airline", Q_NULLPTR));
        label_ziel->setText(QApplication::translate("MainWindow", "Ziel", Q_NULLPTR));
        groupBox_details2->setTitle(QApplication::translate("MainWindow", "Hotelbooking Deatils", Q_NULLPTR));
        label_hotel->setText(QApplication::translate("MainWindow", "Hotel", Q_NULLPTR));
        label_stadt->setText(QApplication::translate("MainWindow", "Stadt", Q_NULLPTR));
        label_raucher->setText(QApplication::translate("MainWindow", "Raucher", Q_NULLPTR));
        menuReiseb_ro->setTitle(QApplication::translate("MainWindow", "Funktionen", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
