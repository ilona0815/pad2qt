
#include "Booking.h"


Booking ::Booking () {
    if (currentID<=this->id){
        currentID=this->id++;}
}
Booking ::Booking (long id, double price, string fromDate, string toDate, long tripID,long customerID,string customerName) {
    this->id=id;
    this->price=price;
    this->fromDate=fromDate;
    this->toDate=toDate;
    this->tripID=tripID;
    this->customerID=customerID;
    this->customerName=customerName;
    
    if (currentID<=this->id){
        currentID=this->id+1;
        }
}
long Booking::getTripID(){
    return tripID;
}

vector<int> Booking ::getEndDate(){
vector<int> v ;
v.push_back(stoi(this->toDate.substr(0, 4)));
v.push_back(stoi(this->toDate.substr(4, 2)));
v.push_back(stoi(this->toDate.substr(6, 2)));
return v;
}
vector<int> Booking ::getStartDate(){
    vector<int> v ;
    v.push_back(stoi(this->fromDate.substr(0, 4)));
    v.push_back(stoi(this->fromDate.substr(4, 2)));
    v.push_back(stoi(this->fromDate.substr(6, 2)));
    return v;
}

char Booking::getType(){
    return this->type;
}

long Booking ::getID(){
    return this->id;
}
double Booking ::getPrice(){
    return this->price;
}
string Booking ::getCustomerName(){
    return this->customerName;
}


long Booking ::getCurrentID(){
    return Booking ::currentID;
}

Booking ::~Booking () {
}



string Booking ::getBigString(){
    string big="";
    big+=to_string(this->id);
    big+="   ";
    big+=to_string(this->price);
    big+="   ";
    big+=this->customerName;
    return big;
}

long Booking ::currentID = 1;
