#include "list.h"

template <class Anything>

List<Anything>::List()
{

}

template <class Anything>
Node<Anything>* List<Anything>::getLast(){
    if(first==nullptr){return first;}
    Node<Anything>* ptr = first;
    while(ptr->next != nullptr){
        ptr=ptr->next;
    }

    return ptr;
}

template <class Anything>
Node<Anything>* List<Anything>::at(int n)
{
    int i=0;
    int size=this->size();
    Node<Anything>* ptr=first;
    while (i!=n) {
        if(i==size){
            cerr<<"size smaller than requested"<<endl;
            throw runtime_error("size smaller");
        }
        ptr=ptr->next;
        i++;

    }
    return ptr;
}

template <class Anything>

int List<Anything>::size()
{
    Node<Anything>* ptr=first;
    int i=0;
    while (ptr!=nullptr) {
        ptr=ptr->next;
        i++;
    }
    return i;
}

template <class Anything>
void List<Anything>::insert(int order, Anything body)
{
    int size=this->size();
    if(order>size){
        cerr<<"attempt to insert into non-existing position"<<endl;
        return;
    }
    if(order==0&&size==0){
        first=new Node<Anything>(body,nullptr,nullptr);
    }
    if(order==0&&size!=0){
        Node<Anything>* tmp = first;
        tmp->prev=new Node<Anything>(body,tmp,nullptr);
        first=tmp->prev;
    }
    if(order==size){
        this->at(size-1)->next=new Node<Anything>(body,nullptr,this->at(size-1));
    }
    if(order<size&&order>0){
        Node<Anything>* tmpLeft = this->at(order-1);
        Node<Anything>* tmpRight = this->at(order);
        Node<Anything>* tmpNew = new Node<Anything>(body, tmpRight,tmpLeft);
        tmpLeft->next=tmpNew;
        tmpRight->prev=tmpNew;
    }
}


template <class Anything>
void List<Anything>::fitInPlace(Anything theBody)
{

    int size = this->size();
    if(size==0){
        this->push_back(theBody); // TODO: make push for object
        return;
    }

    for(int coursor = 0; coursor<(size-1); coursor ++){
        if(theBody.getTripID()<this->at(coursor)->body.getTripID()){//implement < for bookings?
            this->insert(coursor, theBody); // TODO: make insert for object
            return;
        }
        this->push_back(theBody); // TODO: make push for object
    }
}




template <class Anything>
void List<Anything>::push_back(Anything body){
    if(getLast()==nullptr){
        first=new Node<Anything>(body,nullptr,nullptr);
    }else{
        this->getLast()->next=new Node<Anything>(body,nullptr,this->getLast());//warum geht plotzlich kein NULL?
    }
}
template <class Anything>
void List<Anything>::pop(){
    Node<Anything>* tmp = this->getLast();
    this->getLast()->prev->next=nullptr;
    delete tmp;
}

//template class List<BookingTest>;
template class List<Booking>;
