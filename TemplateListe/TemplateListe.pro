TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    list.cpp \
    lobject.cpp \
    bookingtest.cpp \
    Booking.cpp

HEADERS += \
    list.h \
    lobject.h \
    bookingtest.h \
    Booking.h
