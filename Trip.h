

#ifndef TRIP_H
#define TRIP_H
#include "Booking.h"
#include "general.h"
//#include "Customer.h"

class Trip {
public:
    Trip(long,long);
    void addBooking(Booking * theBooking);
    long getID();
    long getCustomerID();
    void printBookings();
    virtual ~Trip();
private:
    long id;
    long customerID;
    vector<Booking *> relatedBookings;
};

#endif /* TRIP_H */

