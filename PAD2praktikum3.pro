#-------------------------------------------------
#
# Project created by QtCreator 2018-05-08T10:51:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PAD2praktikum3
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
#DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
  FlightBooking.cpp \
 mainwindow.cpp\
CarReservation.cpp\
    HotelBooking.cpp\
   TravelAgency.cpp\
Customer.cpp\
Trip.cpp \
    Booking.cpp \
    test.cpp \
    list.cpp \
    node.cpp

HEADERS  += mainwindow.h\
 Customer.h\
 general.h\
Trip.h\
CarReservation.h\
 FlightBooking.h\
 HotelBooking.h\
TravelAgency.h \
    Booking.h \
    test.h \
    list.h \
    node.h


FORMS    += \
    mainwindow.ui

QT  += testlib
